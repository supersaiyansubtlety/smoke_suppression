- 1.2.1 (17 Dec. 2024): Marked as compatible with 1.21.4
- 1.2.0 (30 Nov. 2024): Updated for 1.21.2-1.21.3
- 1.1.1 (28 Jul 2024): Marked as compatible with 1.21.1
- 1.1.0 (11 Jul. 2024):
  - Updated for 1.21!
  - Replaced bundled [CrowdinTranslate](<https://github.com/gbl/CrowdinTranslate>) with optional
  [SSS Translate](<https://modrinth.com/mod/sss-translate>) dependency;
  install [SSS Translate](<https://modrinth.com/mod/sss-translate>) for automatic translation updates
  - Minor internal changes
- 1.0.20 (9 May 2024):
  - Marked as compatible with 1.20.6
  - Made the mod *properly* client-side
  - Improved [Mod Menu](https://modrinth.com/mod/modmenu) integration
  - Minor internal changes
- 1.0.19 (24 Apr. 2024): Marked as compatible with 1.20.5
- 1.0.18 (29 Jan. 2024):
  - Marked as compatible with 1.20.3 and 1.20.4
  - Minor internal changes
- 1.0.17 (5 Oct. 2023): Updated for 1.20.2
- 1.0.16 (14 Jun. 2023): Added 'Invert list behavior' config which makes blocks on the list enable smoke and all other blocks suppress it
- 1.0.15 (14 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.0.14 (21 Mar. 2023): Updated for 1.19.3 and 1.19.4
- 1.0.13 (6 Aug. 2022): Marked as compatible with 1.19.2
- 1.0.12 (28 Jul. 2022): Marked as compatible with 1.19.1
- 1.0.11 (16 Jul. 2022):
  - Fixed a bug that prevented `suppressing_blocks` from loading custom values until config was changed.
  - Fixed error logging when config screen was opened.
- 1.0.10 (22 Jun. 2022): Updated for 1.19!
- 1.0.9-1 (15 Mar. 2022): Lower minimum required version for Fabric API
- 1.0.9 (15 Mar. 2022): 
  
  Updated for 1.18.2

  Cloth Config is now optional

- 1.0.8 (15 Jan. 2022): Fixed crash that could happen when campfires generated in modded structures.
- 1.0.7 (13 Dec. 2021): Marked as compatible with 1.18.1
- 1.0.6 (2 Dec. 2021): Updated for 1.18!

- 1.0.5 (2 Dec. 2021): 
  
  Configs now take effect immediately without the need to restart!

  However existing campfires won't reflect changes until re-placed or re-loaded. 

- 1.0.4 (17 Oct. 2021): Added "Always suppress smoke" option. Thanks to [i0xhex](https://www.curseforge.com/members/i0xhex/projects) for the suggestion.

- 1.0.3 (9 Aug. 2021): 

  - Fixed crash during world generation. Thanks to [TelepathicGrunt](https://gitlab.com/TelepathicGrunt) for help solving this issue!
  - Auto Config is no longer a dependency. 

- 1.0.2 (10 Jul. 2021): Marked as compatible with 1.17.1.

- 1.0.1 (15 Jun. 2021): 

  - Updated for 1.17.
  - Cloth Config and Auto Config (updated) are no longer bundled. Instead, install them separately. 

- 1.0 (26 Jan. 2021): Initial release. 