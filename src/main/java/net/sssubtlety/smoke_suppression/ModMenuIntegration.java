package net.sssubtlety.smoke_suppression;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;
import me.shedaniel.autoconfig.AutoConfig;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import static net.sssubtlety.smoke_suppression.FeatureControl.isConfigLoaded;
import static net.sssubtlety.smoke_suppression.SmokeSuppression.NAMESPACE;

@Environment(EnvType.CLIENT)
public class ModMenuIntegration implements ModMenuApi {
    public static final Text NO_CONFIG_SCREEN_TITLE =
        Text.translatable("text." + NAMESPACE + ".no_config_screen.title");
    public static final Text NO_CONFIG_SCREEN_MESSAGE =
        Text.translatable("text." + NAMESPACE + ".no_config_screen.message");

    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return isConfigLoaded() ?
            parent -> AutoConfig.getConfigScreen(Config.class, parent).get() :
            NoConfigScreen::new;
    }

    public static class NoConfigScreen extends Screen {
        private final Screen parent;

        protected NoConfigScreen(Screen parent) {
            super(NO_CONFIG_SCREEN_TITLE);
            this.parent = parent;
        }

        @Override
        public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
            super.render(graphics, mouseX, mouseY, delta);
            final int horizontalCenter = this.width / 2;

            graphics.drawCenteredShadowedText(
                MinecraftClient.getInstance().textRenderer,
                Util.replace(NO_CONFIG_SCREEN_TITLE, "\\$\\{name\\}", SmokeSuppression.NAME.getString()),
                horizontalCenter, this.height / 10,
                Formatting.WHITE.getColorValue()
            );

            graphics.drawCenteredShadowedText(
                MinecraftClient.getInstance().textRenderer,
                NO_CONFIG_SCREEN_MESSAGE,
                horizontalCenter, this.height / 2,
                Formatting.RED.getColorValue()
            );
        }

        @SuppressWarnings("ConstantConditions")
        @Override
        public void closeScreen() {
            this.client.setScreen(parent);
        }
    }
}