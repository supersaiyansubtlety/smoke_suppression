package net.sssubtlety.smoke_suppression;

import net.minecraft.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SmokeSuppression {
    public static final String NAMESPACE = "smoke_suppression";
    public static final Text NAME = Text.translatable("text." + NAMESPACE + ".name");
    public static final Logger LOGGER = LogManager.getLogger();
}
