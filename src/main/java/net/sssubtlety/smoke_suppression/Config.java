package net.sssubtlety.smoke_suppression;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.ConfigEntry;

import java.util.ArrayList;
import java.util.List;

import static net.sssubtlety.smoke_suppression.SmokeSuppression.NAMESPACE;

@me.shedaniel.autoconfig.annotation.Config(name = NAMESPACE)
public class Config implements ConfigData {
    @ConfigEntry.Gui.Tooltip
    List<String> suppressing_blocks = new ArrayList<>(FeatureControl.Defaults.suppressingBlockIds);

    @ConfigEntry.Gui.Tooltip
    boolean invert_list_behavior = FeatureControl.Defaults.invert_list_behavior;

    @ConfigEntry.Gui.Tooltip
    boolean always_suppress_smoke = FeatureControl.Defaults.always_suppress_smoke;
}
