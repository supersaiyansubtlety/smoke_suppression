package net.sssubtlety.smoke_suppression.mixin_helper;

public interface CampfireBlockEntityMixinAccessor {
    void smoke_suppression$setSmoking(boolean smokey);
    boolean smoke_suppression$isSmoking();
}
