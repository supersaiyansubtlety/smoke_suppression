package net.sssubtlety.smoke_suppression;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigHolder;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.VersionParsingException;
import net.fabricmc.loader.api.metadata.version.VersionPredicate;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.registry.Registries;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import static net.sssubtlety.smoke_suppression.SmokeSuppression.LOGGER;
import static net.sssubtlety.smoke_suppression.Util.isModLoaded;

public final class FeatureControl {
    private FeatureControl() { }

    private static final @Nullable Config CONFIG_INSTANCE;

    private static ImmutableSet<Block> suppressingBlocks = Defaults.suppressingBlocks;

    static {
        if (isModLoaded("cloth-config", ">=6.1.48")) {
            final ConfigHolder<Config> holder = AutoConfig.register(Config.class, GsonConfigSerializer::new);
            CONFIG_INSTANCE = holder.getConfig();
            onConfigChange(holder, CONFIG_INSTANCE);
            holder.registerSaveListener(FeatureControl::onConfigChange);
            holder.registerLoadListener(FeatureControl::onConfigChange);
        } else {
            CONFIG_INSTANCE = null;
        }
    }

    private static ActionResult onConfigChange(ConfigHolder<Config> configHolder, Config config) {
        assert config == CONFIG_INSTANCE;
        suppressingBlocks = buildBlockSetFromStrings(config.suppressing_blocks);
        return ActionResult.CONSUME;
    }

    public interface Defaults {
        ImmutableList<String> suppressingBlockIds = ImmutableList.of(
            Registries.BLOCK.getId(Blocks.HOPPER).toString(),
            Registries.BLOCK.getId(Blocks.RAIL).toString(),
            Registries.BLOCK.getId(Blocks.ACTIVATOR_RAIL).toString(),
            Registries.BLOCK.getId(Blocks.POWERED_RAIL).toString(),
            Registries.BLOCK.getId(Blocks.DETECTOR_RAIL).toString()
        );
        ImmutableSet<Block> suppressingBlocks = buildBlockSetFromStrings(suppressingBlockIds);
        boolean invert_list_behavior = false;
        boolean always_suppress_smoke = false;
    }

    public static boolean shouldSmoke(Supplier<@Nullable Block> blockSupplier) {
        if (CONFIG_INSTANCE == null) return !Defaults.suppressingBlocks.contains(blockSupplier.get());
        if (CONFIG_INSTANCE.always_suppress_smoke) return false;

        return CONFIG_INSTANCE.invert_list_behavior == suppressingBlocks.contains(blockSupplier.get());
    }

    private static ImmutableSet<Block> buildBlockSetFromStrings(List<String> blockStrings) {
        final var blockSetBuilder = ImmutableSet.<Block>builder();
        for (final var string : blockStrings) {
            final var id = Identifier.parse(string);

            Registries.BLOCK.getOrEmpty(id).ifPresentOrElse(
                blockSetBuilder::add,
                () -> LOGGER.warn("No block found for id: {}", id)
            );
        }

        return blockSetBuilder.build();
    }

    public static boolean isConfigLoaded() {
        return CONFIG_INSTANCE != null;
    }

    public static void init() { }
}
