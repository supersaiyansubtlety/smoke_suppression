package net.sssubtlety.smoke_suppression.mixin;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import net.minecraft.block.BlockState;
import net.minecraft.block.CampfireBlock;
import net.minecraft.block.entity.CampfireBlockEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.random.RandomGenerator;
import net.minecraft.world.World;
import net.minecraft.world.WorldView;
import net.minecraft.world.tick.TickSchedulerAccess;

import net.sssubtlety.smoke_suppression.FeatureControl;
import net.sssubtlety.smoke_suppression.mixin_helper.CampfireBlockEntityMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(CampfireBlock.class)
abstract class CampfireBlockMixin {
    @ModifyExpressionValue(
        method = "randomDisplayTick",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/block/BlockState;getOrThrow(Lnet/minecraft/state/property/Property;)" +
                "Ljava/lang/Comparable;"
        )
    )
    private Comparable<Boolean> isLitAndSmoking(
        Comparable<Boolean> lit, BlockState state, World world, BlockPos pos
    ) {
        if ((Boolean) lit && world.getBlockEntity(pos) instanceof CampfireBlockEntity campfireBlockEntity) {
            return ((CampfireBlockEntityMixinAccessor) campfireBlockEntity).smoke_suppression$isSmoking();
        }

        return Boolean.FALSE;
    }

    @Inject(
        method = "getStateForNeighborUpdate",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/block/BlockState;with(Lnet/minecraft/state/property/Property;" +
                "Ljava/lang/Comparable;)Ljava/lang/Object;"
        )
    )
    private void onUpdateFromBelowSetSmoking(
        BlockState state, WorldView world, TickSchedulerAccess tickSchedulerAccess, BlockPos pos,
        Direction direction, BlockPos neighborPos, BlockState neighborState, RandomGenerator random,
        CallbackInfoReturnable<BlockState> cir
    ) {
        if (world.getBlockEntity(pos) instanceof CampfireBlockEntity blockEntity) {
            ((CampfireBlockEntityMixinAccessor) blockEntity).smoke_suppression$setSmoking(
                FeatureControl.shouldSmoke(() -> direction == Direction.DOWN ? neighborState.getBlock() : null)
            );
        }
    }
}
