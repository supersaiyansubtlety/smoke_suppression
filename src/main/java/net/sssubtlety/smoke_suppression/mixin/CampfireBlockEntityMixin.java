package net.sssubtlety.smoke_suppression.mixin;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.CampfireBlockEntity;
import net.minecraft.util.Lazy;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.sssubtlety.smoke_suppression.FeatureControl;
import net.sssubtlety.smoke_suppression.mixin_helper.CampfireBlockEntityMixinAccessor;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(CampfireBlockEntity.class)
abstract class CampfireBlockEntityMixin extends BlockEntity implements CampfireBlockEntityMixinAccessor {
    @SuppressWarnings("deprecation")
    @Unique private Lazy<MutableBoolean> smoking;

    private CampfireBlockEntityMixin() {
        //noinspection DataFlowIssue
        super(null, null, null);
        throw new IllegalStateException("CampfireBlockEntityMixin's dummy constructor called!");
    }

    @Inject(method = "<init>", at = @At("TAIL"))
    private void initFields(BlockPos pos, BlockState state, CallbackInfo ci) {
        //noinspection deprecation
        this.smoking = new Lazy<>(() -> new MutableBoolean(FeatureControl.shouldSmoke(() ->
            this.world == null ? Blocks.AIR : this.world.getBlockState(this.pos.down()).getBlock()
        )));
    }

    @Inject(method = "clientTick", at = @At("HEAD"), cancellable = true)
    private static void cancelIfNotSmoking(
        World world, BlockPos pos, BlockState state, CampfireBlockEntity campfire, CallbackInfo ci
    ) {
        if (!((CampfireBlockEntityMixin)(Object)campfire).smoke_suppression$isSmoking()) {
            ci.cancel();
        }
    }

    @Override
    public void smoke_suppression$setSmoking(boolean smokey) {
        this.smoking.get().setValue(smokey);
    }

    @Override
    public boolean smoke_suppression$isSmoking() {
        return this.smoking.get().booleanValue();
    }
}
